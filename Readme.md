UART pass-through
===============================================
_UART to USB brige using an mbed microcontroller_

Note that this program was written for the LPC1768 mbed board.
If you use another board you may need to redefine the pins
used for the UART connection to the target device.

Install using:

````
mbed import https://bitbucket.org:molluscmicros/uartpassthrough
cd uartpassthrough/
mbed target <TARGET>
mbed compile
````

License
-------

Copyright 2016 Nick Kolpin (nick.kolpin@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
